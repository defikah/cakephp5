<?php
App::uses('AppController', 'Controller');
/**
 * Posts Controller
 *
 * @property Post $Post
 * @property PaginatorComponent $Paginator
 */
class PostsController extends AppController {

	public function beforeFilter() {
	    parent::beforeFilter();
	    $this->Auth->allow();
	    // $this->Auth->allow('index', 'view');
	}

	public $components = array('Paginator');

	public function add() {
		if ($this->request->is('post')) {
			$this->Post->create();
			if ($this->Post->save($this->request->data)) {
				$this->Flash->success(__('The post has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The post could not be saved. Please, try again.'));
			}
		}
		$users = $this->Post->User->find('list');
		$this->set(compact('users'));
	}

	public function ajax_add() {
		$this->layout = 'ajax';
	 //$this->autoRender = false;
	    $response = array('status'=>'failed', 'message'=>'HTTP method not allowed');
	 	if($this->request->is('post')){
	        
	        //get data from request object
	        $data = $this->request->input('json_decode', true);
	        if(empty($data)){
	            $data = $this->request->data;
	        }
	        
	        //response if post data or form data was not passed
	        $response = array('status'=>'failed', 'message'=>'Please provide form data');
	            
	        if(!empty($data)){
	            //call the model's save function
	            if($this->Post->save($data)){
	                //return success
	                $response = array('status'=>'success','message'=>'Product successfully created');
	            } else{
	                 $response = array('status'=>'failed', 'message'=>'Failed to save data');
	             }
	        }
	    }
	        
	    $this->response->type('application/json');
	    $this->response->body(json_encode($response));
	    return $this->response->send();
	}

	public function view($id = null) {
		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}

		$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
		$this->set('post', $this->Post->find('first', $options));
	}

	public function ajax_view($id = null){
	    $this->layout = 'ajax';
	    //$this->autoRender = false;
	    //set default response
	    $response = array('status'=>'failed', 'message'=>'Failed to process request');
	    
	    //check if ID was passed
	    if(!empty($id)){
	        
	        //find data by ID
	        $result = $this->Post->findById($id);
	        if(!empty($result)){
	            $response = array('status'=>'success','data'=>$result);  
	        } else {
	            $response['message'] = 'Found no matching data';
	        }  
	    } else {
	        $response['message'] = "Please provide ID";
	    }
	        
	    $this->response->type('application/json');
	    $this->response->body(json_encode($response));
	    return $this->response->send();
	}

	public function delete($id = null) {
		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Post->delete($id)) {
			$this->Flash->success(__('The post has been deleted.'));
		} else {
			$this->Flash->error(__('The post could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function ajax_delete(){
		$this->layout = 'ajax';
	    $this->autoRender = false;
	    
	    //set default response
	    $response = array('status'=>'failed', 'message'=>'HTTP method not allowed');
	    
	    //check if HTTP method is DELETE
	    if($this->request->is('delete')){
	        //get data from request object
	        $data = $this->request->input('json_decode', true);
	        if(empty($data)){
	            $data = $this->request->data;
	        }
	        
	        //check if product ID was provided
	        if(!empty($data['id'])){
	            if($this->Post->delete($data['id'], true)){
	                $response = array('status'=>'success','message'=>'Product successfully deleted');
	            }
	        }
	    }
	        
	    $this->response->type('application/json');
	    $this->response->body(json_encode($response));
	    return $this->response->send();
	}
	
	public function index() {
		$this->paginate = array(
		'fields' => array(
				'Post.id, 
				Post.title, 
				Post.user_id, 
				User.id, 
				Post.body, 
				Post.created, 
				Post.modified, 
				User.username'
				)
			);

		$data = $this->paginate("Post");
		$this->set('posts', $data);
	}

	public function ajax_index() {
		$this->layout = 'ajax';
		$this->autoRender = false;
		$response = array('status'=>'failed', 'message'=>'HTTP method not allowed');
		if ($this->request->is('post')){
			$result = $this->Post->find('all'); 

			if(!empty($result)){
	          $response = array('status'=>'success','data'=>$result);  
	        } else {
	           $response['message'] = 'Found no matching data';

	        }  
		}
		else {
			$response = array('status'=>'failed', 'message'=>'HTTP method not allowed');
		}

		$this->response->type('application/json');
	    $this->response->body(json_encode($response));
	    return $this->response->send();
	}

	public function edit($id = null) {
		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Post->save($this->request->data)) {
				$this->Flash->success(__('The post has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The post could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
			$this->request->data = $this->Post->find('first', $options);
		}

		$users = $this->Post->User->find('list');
		$this->set(compact('users'));
	}

	public function ajax_update(){
	    //set layout as false to unset default CakePHP layout. This is to prevent our JSON response from mixing with HTML
	   $this->layout = 'ajax';
	   $this->autoRender = false;
	    //set default response
	    $response = array('status'=>'failed', 'message'=>'HTTP method not allowed');
	    
	    //check if HTTP method is PUT
	    if($this->request->is('put')){
	        //get data from request object
	        $data = $this->request->input('json_decode', true);
	        if(empty($data)){
	            $data = $this->request->data;
	        }
	        
	        //check if product ID was provided
	        if(!empty($data['id'])){
	            
	            //set the product ID to update
	            $this->Post->id = $data['id'];
	            if($this->Post->save($data)){
	                $response = array('status'=>'success','message'=>'successfully updated');
	            } else {
	                $response['message'] = "Failed to";
	            }
	        } else {
	            $response['message'] = 'Please provide ID';
	        }
	    }
	        
	    $this->response->type('application/json');
	    $this->response->body(json_encode($response));
	    return $this->response->send();
	}
}
